package com.lab111.labwork4;

/**
 * Special abstract class where is defined a method of drawing of pictures
 * @author Yuriy
 * @version 1.5
 */
public abstract class Shapes {
    /**
     * Method draw, which you can drove elements from primitives
     */
    abstract void draw();
}
