package com.lab111.labwork4;

/**
 * Interface of lines, which defines the value of length of lines
 * @author Yuriy
 * @version 1.5
 */
public interface LineInt {

    /**
     * Method of setting the length of x-component coordinate
     * @param x
     * parameter which transmit the value of length
     */
    void SetX(double x);

    /**
     * Method of setting the length of y-component coordinate
     * @param y
     * parameter which transmit the value of length
     */
    void SetY(double y);
}
