package com.lab111.labwork4;

/**
 * Class of creating an object of Dot which it's coordinates of X and Y
 * @author Yuriy
 * @version 1.5
 */
public class EveryDot extends Shapes implements Dot{

    private int X;
    private int Y;

    /**
     * Implemented method from interface Dot of setting X
     * @param x
     * It's parameter x
     */
    @Override
    public void SetX(int x) {
        this.X = x;
    }

    /**
     * Implemented method from interface Dot of setting Y
     * @param y
     * It's parameter y
     */
    @Override
    public void SetY(int y) {
        this.Y = y;
    }

    /**
     * Inherited method of drawing elements in graphics, in this example is a dot
     */
    @Override
    void draw() {
        System.out.println("Dot: X = "+X+"; Y = "+Y);
    }
}
