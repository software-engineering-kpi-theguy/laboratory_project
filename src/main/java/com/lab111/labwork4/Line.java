package com.lab111.labwork4;

/**
 *Class, where we create a composition object line, from primitives of dots. Adapter pattern used here
 * @author Yuriy
 * @version 1.5
 */
public class Line extends Shapes implements LineInt{

    private double Xl;
    private double Yl;
    private int StartX;
    private int StartY;
    private int EndX;
    private int EndY;

    //Setters of creating starting-ending of line in X, Y coordinates
    public void setStartX(int startX) {
        StartX = startX;
    }

    public void setStartY(int startY) {
        StartY = startY;
    }

    public void setEndX(int endX) {
        EndX = endX;
    }

    public void setEndY(int endY) {
        EndY = endY;
    }

    /**
     * Method of the counting a length in a X-axis
     * @param Sx
     * parameter of starting coordinate of X
     * @param Ex
     * parameter of ending coordinate of X
     * @return
     * Return a number, which equal to a length
     */
    public double Xlength(int Sx, int Ex){
        if(Math.abs(Sx - Ex) > 0){
            return Sx - Ex;
        }
        else if(Math.abs(Ex - Sx) > 0){
            return Ex - Sx;
        }
        return -1;
    }

    /**
     * Method of the counting a length in a Y-axis
     * @param Sy
     * parameter of starting coordinate of Y
     * @param Ey
     * parameter of ending coordinate of Y
     * @return
     * Return a number, which equal to a length
     */
    public double Ylength(int Sy, int Ey){
        if(Sy - Ey > 0){
            return Sy - Ey;
        }
        else if(Ey - Sy > 0){
            return Ey - Sy;
        }
        return -1;
    }

    /**
     * Implemented method of Setting a length field of class
     * @param x
     * parameter of this method
     */
    @Override
    public void SetX(double x) {
        this.Xl = x;
    }

    /**
     * Implemented method of Setting a length field of class
     * @param y
     * parameter of this method
     */
    @Override
    public void SetY(double y) {
        this.Yl = y;
    }

    public Line(int Sx, int Sy, int Ex, int Ey){
        setStartX(Sx);
        setStartY(Sy);
        setEndX(Ex);
        setEndY(Ey);
        SetX(Xlength(StartX, EndX));
        SetY(Ylength(StartY, EndY));
    }

    /**
     * Inherited method of drawing an element in this graphics. There we use a composition of primitive objects - dots.
     */
    @Override
    public void draw() {
        EveryDot dot = new EveryDot();
        double i = 0.0;
        double j = 0.0;
        double k = 0.0;
        boolean f1 = false;
        boolean f2 = false;
        do {
            if (i != Xl){
                i++;
                if (StartX > EndX){
                    dot.SetX((int) (StartX - i));
                }
                else if (EndX > StartX) {
                    dot.SetX((int) (StartX + i));
                }
                if(i*0.1 == Xl){
                    f1 = true;
                }
            }
            if(j != Yl){
                j++;
                if (StartY > EndY){
                    dot.SetY((int) (StartY - j));
                }
                else if (EndY > StartY) {
                    dot.SetY((int) (StartY + j));
                }
                if(j == Yl){
                    f2 = true;
                }
            }
            dot.draw();
            if(f1 && f2)
                break;
            k++;
        }while(k <= Math.sqrt(Math.pow(Xl, 2)+Math.pow(Yl, 2)));
    }
}
