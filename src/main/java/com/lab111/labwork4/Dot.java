package com.lab111.labwork4;

/**
 * Interface of dot, which defines the coordinates in pixels
 * @author Yuriy
 * @version 1.5
 */
public interface Dot {

    /**
     * Method that sets X coordinate
     * @param x
     * parameter of x, which transmit the value of coordinate
     */
    void SetX(int x);

    /**
     * Method that sets Y coordinate
     * @param y
     * parameter of y, which transmit the value of coordinate
     */
    void SetY(int y);

}
