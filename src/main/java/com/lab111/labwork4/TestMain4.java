package com.lab111.labwork4;

/**
 * Main method, where we create an objects of line, to create it in a graphics
 * @author Yuriy
 * @version 1.5
 */
public class TestMain4 {

    public static void main(String[] args){
        Line lin1 = new Line(14, 8, 6, 13);
        Line lin2 = new Line(28, 35, 2, 40);
        lin1.draw();
        lin2.draw();
    }
}
