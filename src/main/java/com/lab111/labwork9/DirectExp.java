package com.lab111.labwork9;

/**
 * Class of Director, which configures construction of Builder objects
 * @author Yuriy
 * @version 3.0
 */
public class DirectExp {
    private Builder builder;

    /**
     * Method, that sets a Builder objects to director class
     * @param built
     * Object of Builder, which saves in field of director
     */
    public void setBuilder (Builder built){
        this.builder = built;
    }

    /**
     * Method, that gets a value of Builder object from field of director
     * @return
     * Value of field of builder
     */
    public Builder getBuilder(){
        return this.builder;
    }

    /**
     * Method, that returns a value of expression, saved in Builder object
     * @return
     * An expression, that saved in builder
     */
    public Expressions getExp(){
        return this.builder.getExp();
    }

    /**
     * Method, that creates operations, that is in concreteBuilder objects. It uses methods, that is in Builder class
     */
    public void createExp(){
        getBuilder().createExpression();
        getBuilder().buildExp();
    }
}
