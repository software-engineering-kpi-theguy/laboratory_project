package com.lab111.labwork9;

/**
 * Abstract class of Builder, that represents an abstract methods for concreteBuilder extended classes
 * @author Yuriy
 * @version 3.0
 */
public abstract class Builder {
    private Expressions exp;

    /**
     * Method, that returns an expression, saved in this class
     * @return
     * The value of expression that in a field
     */
    public Expressions getExp(){
       return exp;
    }

    /**
     * Abstract method, that uses for creating an expressions by subclasses of this
     */
    public abstract void createExpression();

    /**
     * Abstract method, that used by subclasses to form an expresson with it`s fields and elements
     */
    public abstract void buildExp();
}
