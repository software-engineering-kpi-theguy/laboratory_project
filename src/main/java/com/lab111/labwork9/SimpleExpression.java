package com.lab111.labwork9;

/**
 * Class of Sinple expressions, which uses only operands to form it
 * @author Yuriy
 * @version 3.0
 */
public class SimpleExpression extends Expressions {
    private Operand op;

    /**
     * An empty constructor, implemented from abstract expressions, for future operations
     */
    public SimpleExpression(){

    }

    /**
     * Method, that forms value of operands in this class
     * @param operand
     * Operands, that will be saved in field of class
     */
    public void setOp(Operand operand){
        this.op = operand;
    }
}
