package com.lab111.labwork9;

/**
 * Main class, where we make an expression
 * @author Yuriy
 * @version 3.0
 */
public class TestMain9 {
    public static void main (String[] args){
        DirectExp director = new DirectExp();
        SimpleBuilder simple = new SimpleBuilder();
        ComplexBuilder complex = new ComplexBuilder();
        director.setBuilder(simple);
        director.createExp();
        director.setBuilder(complex);
        director.createExp();
    }
}
