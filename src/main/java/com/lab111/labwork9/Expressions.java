package com.lab111.labwork9;

/**
 * Abstract class, that used for generalization of all Expressions subclasses to form it
 * @author Yuriy
 * @version 3.0
 */
public abstract class Expressions {

    /**
     * An empty constructor, that created to form next operations in future
     */
    public Expressions(){

    }
}
