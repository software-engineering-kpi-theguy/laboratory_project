package com.lab111.labwork9;

import java.util.Random;

/**
 * Sub-class of Builder, used to form a simple expressions
 * @author Yuriy
 * @version 3.0
 */
public class SimpleBuilder extends Builder {
    private SimpleExpression simple;

    /**
     * Method, that sets an field of simple expression in it
     * @param exp
     * Element that is needed to save in a field
     */
    public void setSimple (SimpleExpression exp){
        this.simple = exp;
    }

    /**
     * Method, that used for getting a value of field of expression
     * @return
     * The value of this field
     */
    public SimpleExpression getSimple(){
        return this.simple;
    }

    /**
     * Method, that implemented from abstract Builder, and it forms an simple expression
     */
    @Override
    public void createExpression() {
        System.out.println("Creating of simple expression...");
        setSimple(new SimpleExpression());
    }

    /**
     * Method, that is implemented from abstract Builder, to add elements into a simple expression
     */
    @Override
    public void buildExp() {
        System.out.println("Building an expression...Setting an operands...");
        Random rand = new Random();
        boolean r = rand.nextBoolean();
        if(r){
            getSimple().setOp(new Constant());
        }
        else{
            getSimple().setOp(new Variable());
        }
    }
}
