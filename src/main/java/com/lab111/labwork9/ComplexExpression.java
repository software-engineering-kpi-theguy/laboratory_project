package com.lab111.labwork9;

/**
 * Class of complex expressions, that uses expressions and math operations to form it
 * @author Yuriy
 * @version 3.0
 */
public class ComplexExpression extends Expressions {
    private Expressions firstExp;
    private Expressions secondExp;
    private MathOperation math;

    /**
     * An empty constructor, implemented from abstract expressions, for future operations
     */
    public ComplexExpression(){

    }

    /**
     * Method, that sets a value of first expression in complex expression class
     * @param first
     * An expression, that we want to save in this field
     */
    public void setFirstExp(Expressions first){
        this.firstExp = first;
    }

    /**
     * Method, that sets a value of second expression in complex expression class
     * @param second
     * An expression, that we want to save in this field
     */
    public void setSecondExp(Expressions second){
        this.secondExp = second;
    }

    /**
     * Method, that sets a MathOperation of complex expression class
     * @param operation
     * An operation, that we want to save in a field
     */
    public void setMath(MathOperation operation){
        this.math = operation;
    }
}
