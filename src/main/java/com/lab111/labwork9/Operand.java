package com.lab111.labwork9;

/**
 * Abstract class of operands, that generalizes all operands that might be in simple expressions
 * @author Yuriy
 * @version 3.0
 */
public abstract class Operand {
    /**
     * An empty constructor of class, which can be used for future operations
     */
    public Operand(){

    }
}
