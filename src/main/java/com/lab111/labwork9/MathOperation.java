package com.lab111.labwork9;

/**
 * Class of math operations, that represents an operations in mathematics
 * @author Yuriy
 * @version 3.0
 */
public class MathOperation {
    /**
     * Constructor, which shows a thing, that this is a math operations class
     */
    public MathOperation(){
        System.out.println("This is math operation. Using +|-|*|/");
    }
}
