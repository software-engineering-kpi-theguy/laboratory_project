package com.lab111.labwork9;

/**
 * Class of Variables, that represents a variable thing of operand
 * @author Yuriy
 * @version 3.0
 */
public class Variable extends Operand {
    /**
     * Constructor, that displays, that this is a variable
     */
    public Variable(){
        System.out.println("This is Variable. Using a name");
    }
}
