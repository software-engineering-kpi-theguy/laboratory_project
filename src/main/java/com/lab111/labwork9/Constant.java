package com.lab111.labwork9;

/**
 * Class of Constants, that represents a constant value of operand
 * @author Yuriy
 * @version 3.0
 */
public class Constant extends Operand {
    /**
     * Constructor, that displays, that this is a constant
     */
    public Constant(){
        System.out.println("This is constant. Using a number");
    }
}
