package com.lab111.labwork9;

/**
 * Sub-class of Builder, used to form a complex expressions
 * @author Yuriy
 * @version 3.0
 */
public class ComplexBuilder extends Builder{
    private ComplexExpression complex;

    /**
     * Method, that sets a value of complex expression in this class field
     * @param exp
     * An expression, that we save in a field of builder
     */
    public void setComplex(ComplexExpression exp){
        this.complex = exp;
    }

    /**
     * Method, that gets a value of complex expression from field of this class
     * @return
     * A value of expression, saved in a field of class
     */
    public ComplexExpression getComplex(){
        return this.complex;
    }

    /**
     * Method, that implemented from abstract Builder, and it forms an complex expression
     */
    @Override
    public void createExpression() {
        System.out.println("Creating of complex expression...");
        setComplex(new ComplexExpression());
    }

    /**
     * Method, that is implemented from abstract Builder, to add elements into a complex expression
     */
    @Override
    public void buildExp() {
        System.out.println("Building an expression...Setting first expression...");
        System.out.println("Setting second expression...");
        System.out.println("Setting math operation between...");
        getComplex().setFirstExp(new SimpleExpression());
        getComplex().setSecondExp(new ComplexExpression());
        getComplex().setMath(new MathOperation());
    }
}
