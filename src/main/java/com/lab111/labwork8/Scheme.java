package com.lab111.labwork8;

/**
 * Static class of scheme, that represents pattern Singleton, to become one example
 * @author Yuriy
 * @version 2.6
 */
public class Scheme {
    private static Scheme schemeInstance;

    /**
     * An empty constructor, where we can add creating of scheme.
     */
    public Scheme(){

    }

    /**
     * Setter of field of instance, where we save object of this class
     * @param scheme
     */
    public static void setSchemeInstance(Scheme scheme){
        schemeInstance = scheme;
    }

    /**
     * Getter of field of instance. We take object of class from itself. If it matches null, it's creates automatically.
     * @return
     * Value of field of instance, or just an object of this class. It's needed, to save individuality.
     */

    public static Scheme getSchemeInstance() {
        if(schemeInstance == null){
            System.out.println("Instance = null. Object of class didn't exist. It`s creates automatically.");
            setSchemeInstance(new Scheme());
        }
        else
        {
            System.out.println("Instance = object of Scheme.");
        }
        return schemeInstance;
    }
}
