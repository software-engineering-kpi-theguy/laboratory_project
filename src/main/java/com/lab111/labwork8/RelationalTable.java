package com.lab111.labwork8;

/**
 * Class of relational table, where we save examples of scheme and Validator.
 * @author Yuriy
 * @version 2.6
 */
public class RelationalTable {
    private Scheme scheme;
    private Validator validator;

    /**
     * An empty constructor of class, where we can put elements of this class.
     */
    public RelationalTable() {

    }

    /**
     * Setter of scheme, sets another one scheme to this relational table.
     * @param scheme
     * Scheme, that can be saved in field of class.
     */
    public void setScheme(Scheme scheme) {
            this.scheme = scheme;
        }

    /**
     * Getter of scheme, that we can take from field of class.
     * @return
     * Value of this field, or just a scheme.
     */
    public Scheme getScheme(){
            return scheme;
    }

    /**
     * Method, that sets a value of Validator if it needed. But for the task, it's just necessity
     * @param validator
     * Validator, that we can save in it's field
     */
    public void setValidator(Validator validator) {
            this.validator = validator;
        }

}
