package com.lab111.labwork8;

/**
 * Main class, where we create an relational table
 * @author Yuriy
 * @version 2.6
 */
public class TestMain8 {
    public static void main (String[] args){
        RelationalTable table1 = new RelationalTable();
        Scheme scheme1 = new Scheme();
        table1.setScheme(scheme1);
        table1.getScheme().getSchemeInstance();
        Scheme scheme2 = new Scheme();
        table1.setScheme(scheme2);
        table1.getScheme().getSchemeInstance();
        Scheme scheme3 = new Scheme();
        table1.setScheme(scheme3);
        table1.getScheme().getSchemeInstance();
    }
}
