package com.lab111.labwork2;

/**
 * The Class Cl1 implements interface If1, inherits class Cl2 and aggregate interface If1 and If2
 * @author Yuriy
 * @version 1.1
 */
public class Cl1 extends Cl2 implements If1 {

    /**
     * Class Cl2 aggregates interface If1 using a field if1, and aggregates interface If2 with a field if2
     */

    If1 if1;
    If2 if2;

    /**
     * We do a constructor of class Cl1 with empty parameters, and inside assign fields if1 and if2 to null
     */
    public Cl1(){
        if1 = null;
        if2 = null;
    }

    /**
     * Realization of method math1 from interface If1 in class Cl1
     */
    @Override
    public void math1() {
        System.out.println("Class Cl1; Method math1;");
    }

    /**
     * Realization of method math3 from interface If3 in class Cl1
     */
    @Override
    public void math3() {
        System.out.println("Class Cl1; Method math3.");
    }

    /**
     * Realization of method math2 from interface If2 in class Cl1
     */
    @Override
    public void math2() {
        System.out.println("Class Cl1; Method math2;");
    }
}
