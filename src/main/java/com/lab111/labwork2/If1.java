package com.lab111.labwork2;

/**
 * This interface If1 inherits interface If2 and defines the method math1
 * @author Yuriy
 * @version 1.1
 */
public interface If1 extends If2 {
    public void math1();
}
