package com.lab111.labwork2;

/**Main class of Lab
 * @author Yuriy
 * @version 1.1
 */
public class TestMain2 {
    /**
     * main - method runs the program to perform
     * @param args - parameters from command line
     */
    public static void main(String[] args){
        Cl1 cl1 = new Cl1();
        cl1.math1();
        cl1.math2();
        cl1.math3();
        Cl2 cl2 = new Cl2();
        cl2.math2();
        cl2.math3();
        Cl3 cl3 = new Cl3();
        cl3.math3();
    }
}
