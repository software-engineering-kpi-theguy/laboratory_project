package com.lab111.labwork2;

/**
 * This interface If2 inherits interface If3 and defines the method math2
 * @author Yuriy
 * @version 1.1
 */
public interface If2 extends If3{
    public void math2();
}
