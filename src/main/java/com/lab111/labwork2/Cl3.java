package com.lab111.labwork2;

/**
 * The Class Cl3 implements interface If3
 * @author Yuriy
 * @version 1.1
 */
public class Cl3 implements If3 {

    /**
     * Realization of method math3 from interface If3 in class Cl3
     */
    @Override
    public void math3() {
        System.out.println("Class Cl3; Method math3.");
    }
}
