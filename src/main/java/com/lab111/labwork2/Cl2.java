package com.lab111.labwork2;

/**
 * The Class Cl2 implements interface If2 and aggregate class Cl3
 * @author Yuriy
 * @version 1.1
 */
public class Cl2 implements If2 {

    /**
     * Class Cl2 aggregates class Cl3 using a field cl3
     */
    Cl3 cl3;

    /**
     * We do a constructor of class Cl2 with empty parameters, and inside assign field cl3 to null
     */
    public Cl2(){
        cl3 = null;
    }

    /**
     * Realization of method math2 from interface If2 in class Cl2
     */
    @Override
    public void math2() {
        System.out.println("Class Cl2; Method math2;");
    }

    /**
     * Realization of method math3 from interface If3 in class Cl3
     */
    @Override
    public void math3() {
        System.out.println("Class Cl2; Method math3.");
    }
}
