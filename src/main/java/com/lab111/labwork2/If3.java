package com.lab111.labwork2;

/**
 * This interface If3 defines the method math3
 * @author Yuriy
 * @version 1.1
 */
public interface If3 {
    public void math3();
}
