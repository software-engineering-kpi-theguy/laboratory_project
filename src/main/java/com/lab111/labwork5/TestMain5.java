package com.lab111.labwork5;

/**
 * Main method, where we creates objects for table, and checking working of pattern of Observer
 * @author Yuriy
 * @version 2.0
 */
public class TestMain5 {

    public static void main(String[] args){
        Functions f1 = new Functions("x^a");
        Functions f2 = new Functions("log(n)(x)");
        Functions f3 = new Functions("n^x");
        Functions f4 = new Functions("k*x");

        Types typ1 = new Types("parabola", 3);
        Types typ2 = new Types("line", -0.452);
        Types typ3 = new Types("parabola", 0.164);
        Types typ4 = new Types("giperbola", -4.6);
        Types typ5 = new Types("line", 7.86);
        Types typ6 = new Types("parabola", 10);
        Types typ7 = new Types("parabola", 2.7);
        Types typ8 = new Types("giperbola", -7);

        RelTable tab = new RelTable();

        f1.addObs(tab);
        f2.addObs(tab);
        f3.addObs(tab);
        f4.addObs(tab);

        tab.add(f1, typ6);
        tab.add(f4, typ5);
        tab.add(f3, typ3);
        tab.add(f1, typ8);
        tab.add(f2, typ6);
        tab.add(f3, typ7);
        tab.add(f1, typ7);
        tab.add(f4, typ2);
        tab.add(f2, typ7);
        tab.add(f3, typ6);

        tab.printTable();
        f4.deleteFunc(f3);
        f4.renewalFunc(f1, f2);
    }
}
