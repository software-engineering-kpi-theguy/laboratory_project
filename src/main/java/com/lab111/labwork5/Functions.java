package com.lab111.labwork5;

/**
 * Class of Functions, a group of objects, which will be controlled by Observers
 * @author Yuriy
 * @version 2.0
 */
public class Functions extends Subject {
    private String nameOfFunc;

    /**
     * Getter of the NameofFunc
     * @return
     */
    public String getNameOfFunc() {
        return nameOfFunc;
    }

    /**
     * Setter of the NameOfFunc
     * @param nameOfFunc
     * parameter of the field
     */
    public void setNameOfFunc(String nameOfFunc) {
        this.nameOfFunc = nameOfFunc;
    }

    /**
     * Cobstructor of the class, where transmitted attribute setting to the class
     * @param nam
     * parameter of the class
     */
    public Functions(String nam){
        setNameOfFunc(nam);
    }

    /**
     * Function, that deletes a group for table, and notifies it to the observer
     * @param f
     * Function, that is going to delete
     */
    public void deleteFunc(Functions f){
        notifyObs(f);
    }

    /**
     * Method of reneweling of function by another one and notifying it to the observer
     * @param f1
     * Old function, thar will be replaced
     * @param f2
     * New function, that replaces
     */
    public void renewalFunc(Functions f1, Functions f2){
        notifyObs(f1, f2);
    }
}
