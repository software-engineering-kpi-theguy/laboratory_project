package com.lab111.labwork5;

/**
 * Class of types, an elements-objects in groups of functions, which can be controlled by Observer
 * @author Yuriy
 * @version 2.0
 */
public class Types {
    private String typName;
    private double atrOfType;
    private int func;

    /**
     * Getter of TypName
     * @return
     * Field value of TypName
     */
    public String getTypName() {
        return typName;
    }

    /**
     * Setter of TypName
     * @param typName
     * Sets it to the field
     */
    public void setTypName(String typName) {
        this.typName = typName;
    }

    /**
     * Getter of AtrOfType
     * @return
     * Field value of AtrOfType
     */
    public double getAtrOfType() {
        return atrOfType;
    }

    /**
     * Setter of AtrOfType
     * @param atrOfType
     * Sets it to the field
     */
    public void setAtrOfType(double atrOfType) {
        this.atrOfType = atrOfType;
    }

    /**
     * Getter of position of Function
     * @return
     * Field value of position of Function
     */
    public int getFunc() {
        return func;
    }

    /**
     * Setter of position of Function
     * @param func
     * Field value of position of Function
     */
    public void setFunc(int func) {
        this.func = func;
    }

    /**
     * Constructor, that allow to create objects of Types
     * @param typName
     * Attribute of field#1
     * @param atrOfType
     * Attribute of field#2
     */
    public Types(String typName, double atrOfType) {
        setTypName(typName);
        setAtrOfType(atrOfType);
    }
}
