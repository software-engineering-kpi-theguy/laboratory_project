package com.lab111.labwork5;

import java.util.*;

/**
 * Class of Subject, where we controlling the working of interface of Observer
 * @author Yuriy
 * @version 2.0
 */
public class Subject {
    private List obs;

    /**
     * Constructor of the class, which creates a zero ArrayList of observers
     */
    public Subject(){
        obs = new ArrayList();
    }

    /**
     * Method, that adds another one observer to the group of objects
     * @param ob
     * Observer, that is adding ti the list
     */
    public void addObs(Observer ob){
        if(!obs.contains(ob)){
            obs.add(ob);
        }
    }

    /**
     * Method, which deletes any one observer from list
     * @param ob
     * Observer, that is hoing to be deleted
     */
    public void deleteObs(Observer ob){
        obs.remove(ob);
    }

    /**
     * Method, which gives for observer a command, of updating something in objects
     * @param ob1
     * Object1, which is changing
     * @param ob2
     * Object2, which is changing old one
     */
    public void notifyObs(Object ob1, Object ob2){
        Object arr[] = obs.toArray();

        for(int i = arr.length - 1; i >= 0; i--){
            ((Observer)arr[i]).update(this, ob1, ob2);
        }
    }

    /**
     * Method, which give for observer a command, of updating something in one object
     * @param ob
     * Object, which is changing
     */
    public void notifyObs(Object ob){
        Object arr[] = obs.toArray();

        for(int i = arr.length - 1; i >= 0; i--){
            ((Observer)arr[i]).update(this, ob);
        }
    }

    /**
     * Method, that allows to delete all of observers of the group of objects
     */
    public void deleteObs(){
        for(int i = obs.size() - 1; i >= 0; i--){
            obs.remove(i);
        }
    }
}
