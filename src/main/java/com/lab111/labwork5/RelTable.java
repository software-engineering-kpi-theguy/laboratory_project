package com.lab111.labwork5;

import java.util.*;

/**
 * Class of creating of Relation Table using Observers
 * @author Yuriy
 * @version 2.0
 */
public class RelTable implements Observer {

    private ArrayList<Functions> func = new ArrayList<>();
    private ArrayList<Types> typ = new ArrayList<>();

    /**
     * Method, that adds a new function to the table
     * @param f
     * A new function to table
     */
    public void addFunc(Functions f){
        func.add(f);
    }

    /**
     * Method, that adds a new function to the table at the position pos
     * @param f
     * A new function to table
     * @param pos
     * Posiition, where new function will be
     */
    public void addFunc(Functions f, int pos){
        func.add(pos, f);
    }

    /**
     * Method, that adds a new type of function to the table
     * @param t
     * A new type to table
     */
    public void addTyp(Types t){
        typ.add(t);
    }

    /**
     * Method of adding a type to that function in a table
     * @param f
     * A function, which will be a type as parameter
     * @param t
     * A type, that adds to function column
     */
    public void add(Functions f, Types t){
        if(!func.contains(f)){
            addFunc(f);
        }
        t.setFunc(func.indexOf(f));
        addTyp(t);
    }

    /**
     * Method, that deletes Types, when function is deleted from table
     * @param f
     * Function, which deletes from table
     */
    public void DeleteFuncWithTypes(Functions f){
        if (func.contains(f)){
            int buf = func.indexOf(f);
            for (int i = 0; i < 2; i++){
                for (int j = 0; j < typ.size(); j++){
                    if(typ.get(i).getFunc() == buf){
                        typ.remove(i);
                    }
                }
            }
            for (int i = 0; i < typ.size(); i++){
                if(typ.get(i).getFunc() > buf){
                    typ.get(i).setFunc(typ.get(i).getFunc() - 1);
                }
            }
            func.remove(f);
        }
        else{
            System.out.println("There is not such function in table!");
        }
    }

    /**
     * Method, where specific function replaces by another one
     * @param f
     * An old function to replace
     * @param f1
     * A new function that replaces
     */
    public void replacementFunc (Functions f, Functions f1){
        if(func.contains(f)){
            func.set(func.indexOf(f), f1);
        }
        else{
            System.out.println("There is not such function in table!");
        }
    }

    /**
     * Method of printing the whole table to the screen
     */
    public void printTable(){
        if (typ.size() < func.size()){
            throw new SecurityException();
        }

        for (int i = 0; i < typ.size(); i++){
            System.out.println("Function name: "+((Functions)func.get(((Types)typ.get(i)).getFunc())).getNameOfFunc());
            System.out.println("Type: "+typ.get(i).getTypName());
            System.out.println("Attribute of type: "+typ.get(i).getAtrOfType());
        }
        System.out.println("Functions: "+func.size());
    }

    /**
     * Method of updating, which is implemented from interface of Observer
     * @param sbj
     * Subject of the observing
     * @param obj
     * Object, which method is considered when updating
     */
    @Override
    public void update(Subject sbj, Object obj) {
        DeleteFuncWithTypes((Functions) obj);
        printTable();
    }

    /**
     * Method of updating, which is implemented from interface if Observer
     * @param sbj
     * Subject of the observing
     * @param obj1
     * Object1, which method is considered when updating
     * @param obj2
     * Object2, which method is considered when updating
     */
    @Override
    public void update(Subject sbj, Object obj1, Object obj2) {
        replacementFunc((Functions)obj1, (Functions) obj2);
        printTable();
    }
}
