package com.lab111.labwork5;

/**
 * Interface of Observer, allowing to control all of objects from one group
 * @author Yuriy
 * @version 2.0
 */
public interface Observer {
    /**
     * Method, that is calling, when an objects is changing
     * @param sbj
     * Parameter of observable object
     * @param obj
     * Object, that transfered from one of the class, when something is changing
     */
    void update (Subject sbj, Object obj);

    /**
     * Nethod, that is calling, when an object is changing
     * @param sbj
     * Parameter of observable object
     * @param obj1
     * Object#1, that transfered from one of the class, when something is changing
     * @param obj2
     * Object#2, that transfered from one of the class, when something is changing
     */
    void update (Subject sbj, Object obj1, Object obj2);

}
