package com.lab111.labwork6;

/**
 * Concrete class of Visitor, which makes a methods for visiting
 * @author Yuriy
 * @version 2.2
 */
public class ConcreteVisitor implements Visitor {
    private double sumpower = 0;

    /**
     * Method of getting the value of field of complete power, that was checked while visiting
     * @return
     * The value of total power now
     */
    public double getSumpower() {
        return sumpower;
    }

    /**
     * Method of setting the value of field of complete power, which is now checking in visiting
     * @param sumpower
     * Tthe value to set, in this visitation
     */
    public void setSumpower(double sumpower) {
        this.sumpower = sumpower;
    }

    /**
     * Overriding of method of visiting of processor, to calculate his power in system
     * @param p
     * Object of Processor, to take form him his power
     */
    @Override
    public void visitProc(Processor p) {
        double buf = getSumpower() + p.getPower();
        setSumpower(buf);
    }

    /**
     * Overriding of method of visiting of RAM memory, to calculate his power in system
     * @param ram
     * Object of RAM, to take from him his power
     */
    @Override
    public void visitRAM(RAM ram) {
        double buf = getSumpower() + ram.getPower();
        setSumpower(buf);
    }

    /**
     * Overriding of method of visiting of Videocard, to calculate his power in system
     * @param v
     * Object of Videocard, to take from him his power
     */
    @Override
    public void visitVideo(Videocard v) {
        double buf = getSumpower() + v.getPower();
        setSumpower(buf);
    }

    /**
     * Overriding of method of visiting of Motherboard, to calculate his power in system
     * @param m
     * Object of Motherboard, to take from him his power
     */
    @Override
    public void visitMother(Motherboard m) {
        double buf = getSumpower() + m.getPower();
        setSumpower(buf);
    }

    /**
     * Method, that displays tha value of power, that is in field sumpower
     */
    public void genPower(){
        System.out.println("General power = "+getSumpower());
    }
}
