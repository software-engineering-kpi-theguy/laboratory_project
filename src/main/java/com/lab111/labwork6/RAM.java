package com.lab111.labwork6;

/**
 * Class of RAM memory, an element of Structure
 * @author Yuriy
 * @version 2.2
 */
public class RAM extends Structure {

    /**
     * Constructor, that sets the value of this class from Structure
     * @param pow
     * A field to set a value
     */
    public RAM(double pow) {
        super(pow);
    }

    /**
     * Method, that checks if this object of class is visited
     * @param v
     * Interface of visitor, that checks it
     */
    @Override
    public void Accept(Visitor v) {
       v.visitRAM(this);
    }
}
