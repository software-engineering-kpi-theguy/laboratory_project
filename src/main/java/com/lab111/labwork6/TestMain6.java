package com.lab111.labwork6;

import java.util.ArrayList;

/**
 * Main class, where we form a list of component of computer, and check visiting of objects
 * @author Yuriy
 * @version 2.2
 */
public class TestMain6 {
    public static void main (String[] args){
        ArrayList<Structure> comp = new ArrayList<>();
        Processor pr = new Processor(134.5);
        RAM ram = new RAM(76.4);
        Videocard vid = new Videocard(102.8);
        Motherboard mot = new Motherboard(88.2);
        ConcreteVisitor cv = new ConcreteVisitor();

        comp.add(pr);
        comp.add(ram);
        comp.add(vid);
        comp.add(mot);

        for(int i = 0; i < comp.size(); i++){
            comp.get(i).Accept(cv);
        }
        cv.genPower();
    }
}
