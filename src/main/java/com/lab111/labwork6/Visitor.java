package com.lab111.labwork6;

/**
 * Interface of Visitor, which structures visiting elements of list
 * @author Yuriy
 * @version 2.2
 */
public interface Visitor {
    /**
     * Method of visiting of processor, which checks if this object was visited
     * @param p
     * Object of Processor to checking
     */
    void visitProc (Processor p);

    /**
     * Method of vistiing of RAM memory, which checks if this object was visited
     * @param ram
     * Object of RAM to checking
     */
    void visitRAM (RAM ram);

    /**
     * Method of visiting of Videocard, which checks if this object was visited
     * @param v
     * Object of Videocard to checking
     */
    void visitVideo (Videocard v);

    /**
     * Method of visiting of Motherboard, which checks if this object was visited
     * @param m
     * Object of Motherboard to checking
     */
    void visitMother (Motherboard m);
}
