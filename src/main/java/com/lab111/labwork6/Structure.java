package com.lab111.labwork6;

/**
 * Abstract class of Structuring the elements of system
 * @author Yuriy
 * @version 2.2
 */
public abstract class Structure {
    private double power;

    /**
     * Method, that gives a value of power of each element from this structure
     * @return
     * Value of field power, of each element
     */
    public double getPower() {
        return power;
    }

    /**
     * Method, that sets power of each element, while we creating an object of Structure
     * @param power
     * Value of field, to set into each object
     */
    public void setPower(double power) {
        this.power = power;
    }

    /**
     * Constructor of class, to set a field of it
     * @param pow
     * Value of field, to set into this class or element
     */
    public Structure (double pow){
        setPower(pow);
    }

    /**
     * Abstract method, that checks if this object was visited
     * @param v
     * Interface of visitor, that this checks
     */
    public abstract void Accept(Visitor v);
}
