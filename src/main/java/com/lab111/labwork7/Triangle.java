package com.lab111.labwork7;

/**
 * Claas of triangle, that extends a Figure
 * @author Yuriy
 * @version 2.3
 */
public class Triangle extends Figures {
    public Triangle() {
        super("Triangle");
    }
}
