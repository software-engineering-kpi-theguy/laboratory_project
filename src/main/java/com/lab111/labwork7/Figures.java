package com.lab111.labwork7;

/**
 * Class of figures, where we determines a fields of it
 * @author Yuriy
 * @version 2.3
 */
public class Figures {
    private String color;
    private String name;
    private double x;
    private double y;
    private double scale;

    //Getters and setters of fields
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    /**
     * Constructor for extended figures
     * @param nam
     * Names of this figures
     */
    public Figures(String nam) {
        setName(nam);
    }

    /**
     * Sets a state of this figure, using setters
     * @param colr
     * Field of color of figure
     * @param x
     * X coordinate field of figure
     * @param y
     * Y coordinate field of figure
     * @param scal
     * Scaling field of figure
     */
    public void setState(String colr, double x, double y, double scal) {
        setColor(colr);
        setX(x);
        setY(y);
        setScale(scal);
    }

    /**
     * Method that returns a value of new state, using getters
     * @return
     * Object of Memento, that can be saved in Caretaker storage
     */
    public Object returnState (){
        return (Object) new Memento(getColor(), getX(), getY(), getScale());
    }

    /**
     * Restore a state from Caretaker using memento
     * @param obj
     * Object of memento, that will be restored
     */
    public void restoreState(Object obj){
        Memento mem = (Memento) obj;
        setColor(mem.getColor());
        setX(mem.getX());
        setY(mem.getY());
        setScale(mem.getScale());
    }

    /**
     * Method, that shows a state of figure now
     */
    public void showFigure(){
        System.out.println("Figure : name "+getName()+" , it parameters: color = "+getColor()+" , X = "+getX()+" , Y = "+getY()+" , Scale = "+getScale());
    }
}
