package com.lab111.labwork7;

import java.util.ArrayList;

/**
 * Class of storage of objects, that will be available for restoring
 * @author Yuriy
 * @version 2.3
 */
public class FigCaretaker {
    private ArrayList storage = new ArrayList();

    /**
     * Method of saving the state of Figure
     * @param st
     * Parameter of object to save
     */
    public void setState (Object st){
        storage.add(st);
    }

    /**
     * Method restores a state of Figure, that was in a past. Also it deletes it in a list
     * @return
     * Previous state of figure
     */
    public Object getState() {
        Object buff;
        buff = storage.get(storage.size() - 1);
        storage.remove(storage.size() - 1);
        return buff;
    }
}
