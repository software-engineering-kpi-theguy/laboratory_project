package com.lab111.labwork7;

/**
 * Main class of vector graphics
 * @author Yuriy
 * @version 2.3
 */
public class TestMain7 {
    public static void main (String[] args){
        Line lin = new Line();
        Ellipse el = new Ellipse();
        Triangle tri = new Triangle();
        FigCaretaker linc = new FigCaretaker();
        FigCaretaker elc = new FigCaretaker();
        FigCaretaker tric = new FigCaretaker();
        lin.setState("Red", 4.5, 8.7, 12);
        tri.setState("Yellow", 2.6, 9.5, 11);
        el.setState("Blue", 1.7, 6.9, 15);
        lin.showFigure();
        tri.showFigure();
        el.showFigure();
        linc.setState(lin.returnState());
        lin.setState("Green", 4.2, 7.4, 11);
        lin.showFigure();
        lin.restoreState(linc.getState());
        lin.showFigure();
        tric.setState(tri.returnState());
        tri.setState("Pink", 3.2, 5.6, 18);
        tri.showFigure();
        tri.restoreState(tric.getState());
        tri.showFigure();
        elc.setState(el.returnState());
        el.setState("Cyan", 7.5, 0.6, 56);
        el.showFigure();
        el.restoreState(elc.getState());
        el.showFigure();
    }
}
