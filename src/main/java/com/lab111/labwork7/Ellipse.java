package com.lab111.labwork7;

/**
 * Class of ellipse, that extends a Figure
 * @author Yuriy
 * @version 2.3
 */
public class Ellipse extends Figures {
    public Ellipse() {
        super("Ellipse");
    }
}
