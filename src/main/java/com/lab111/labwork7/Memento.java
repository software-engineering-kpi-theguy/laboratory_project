package com.lab111.labwork7;

/**
 * Class of Memento, where is saved a state of object
 * @author Yuriy
 * @version 2.3
 */
public class Memento {
    private String color;
    private double x;
    private double y;
    private double scale;

    //Getters and setters of fields
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getScale() {
        return scale;
    }

    public void setScale(double scale) {
        this.scale = scale;
    }

    /**
     * Constructor that returns a state, whih is saved in Memento
     * @param color
     * Field of color of figure
     * @param x
     * X coordinate field of figure
     * @param y
     * Y coordinate field of figure
     * @param scal
     * Scaling field of figure
     */
    public Memento(String color, double x, double y, double scal){
        setColor(color);
        setX(x);
        setY(y);
        setScale(scal);
    }


}
