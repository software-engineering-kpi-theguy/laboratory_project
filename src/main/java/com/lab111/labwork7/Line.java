package com.lab111.labwork7;

/**
 * Class of line, that extends a Figure
 * @author Yuriy
 * @version 2.3
 */
public class Line extends Figures {
    public Line() {
        super("Line");
    }
}
