package com.lab111.labwork3;

/**
 * Leaf class, a dot in the field
 * @author Yuriy
 * @version 1.2
 */
public class Dot extends Graphics {

    /**
     * Constructor of Dot, that gives a coordinates of it
     * @param EntX
     * @param EntY
     */
    public Dot(double EntX, double EntY){
        X = EntX;
        Y = EntY;
    }
    @Override
    public void draw() {
        System.out.println("Dot: x = "+X+" ; y = "+Y);
    }
}
