package com.lab111.labwork3;

/**
 * Leaf class, a Straight Line in the field
 * @author Yuriy
 * @version 1.2
 */
public class Line extends Graphics {

    /**
     * Constructor of Line, which contains a starting and ending X, Y coordinates
     * @param StartX
     * @param StartY
     * @param EndX
     * @param EndY
     */
    public Line(double StartX, double StartY, double EndX, double EndY){
        X = StartX;
        Y = StartY;
        width = EndX - StartX;
        height = EndY - StartY;
    }

    @Override
    public void draw() {
        System.out.println("Line: X = "+X+" ; Y = "+Y+" ; Width = "+width+" Height = "+height);
    }
}
