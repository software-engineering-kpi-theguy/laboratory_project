package com.lab111.labwork3;

/**
 * A Leaf class, a rectangle in the field
 * @author Yuriy
 * @version 1.2
 */
public class Rectangle extends Graphics{

    /**
     * Constructor of Rectangle, which contains a up left coordinates and down left coordinates of X, Y to draw a rectangle
     * @param LeftUpX
     * @param LeftUpY
     * @param RightDownX
     * @param RightDownY
     */
    public Rectangle(double LeftUpX, double LeftUpY, double RightDownX, double RightDownY){
        X = LeftUpX;
        Y = LeftUpY;
        width = RightDownX - LeftUpX;
        height = RightDownY - LeftUpY;
    }

    @Override
    public void draw() {
        System.out.println("Rectangle: X = "+X+" ; Y = "+Y+" ; Width = "+width+" Height = "+height);
    }
}
