package com.lab111.labwork3;

/**
 * A Leaf class, an ellipse in the field
 * @author Yuriy
 * @version 1.2
 */
public class Ellipse extends Graphics {

    /**
     * Constructor of Ellipse, which contains a centre coordinate, and radiuses of ellipse
     * @param CentrX
     * @param CentrY
     * @param ellWidth
     * @param ellHeight
     */
    public Ellipse(double CentrX, double CentrY, double ellWidth, double ellHeight){
        X = CentrX;
        Y = CentrY;
        width = ellWidth;
        height = ellHeight;
    }
    @Override
    public void draw() {
        System.out.println("Ellipse: X = "+X+" ; Y = "+Y+" ; Width = "+width+" ; Height = "+height);
    }
}
