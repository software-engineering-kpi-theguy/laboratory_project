package com.lab111.labwork3;

import java.util.ArrayList;

/**
 * Composite class, where all graphic elements are storing
 * @author Yuriy
 * @version 1.2
 */
public class Composite extends Graphics {

    /**
     * Constructor of class, where initialization of ArrayList is
     */
    public Composite(){
        initializeElements();
    }

    /**
     * Overrides the method of getX, to get coordinate X from element of ArrayList
     * @return coordinate X
     */
    @Override
    public double getX(){
        ArrayList<Graphics> graphics = getElements();
        if(graphics != null){
            if(!graphics.isEmpty()){
                double bufX = graphics.get(0).getX();
                for(int i = 0; i < graphics.size(); i++){
                    if(graphics.get(i).getX() < bufX)
                        bufX = graphics.get(i).getX();
                }
                X = bufX;
                return X;
            }
        }
        return -1;
    }

    /**
     * Overrides the method of getY, to get coordinate Y from element of ArrayList
     * @return coordinate Y
     */
    @Override
    public double getY(){
        ArrayList<Graphics> graphics = getElements();
        if(graphics != null){
            if(!graphics.isEmpty()){
                double bufY = graphics.get(0).getY();
                for(int i = 0; i < graphics.size(); i++){
                    if(graphics.get(i).getY() < bufY)
                        bufY = graphics.get(i).getY();
                }
                Y = bufY;
                return Y;
            }
        }
        return -1;
    }

    /**
     * Overrides the method of getWidth, to get width from element of ArrayList
     * @return width
     */
    @Override
    public double getWidth(){
        ArrayList<Graphics> graphics = getElements();
        if(graphics != null){
            if(!graphics.isEmpty()){
                double bufWidth = graphics.get(0).getWidth() + graphics.get(0).getX();
                for(int i = 0; i < graphics.size(); i++){
                    if(graphics.get(i).getWidth() + graphics.get(i).getX() > bufWidth)
                        bufWidth = graphics.get(i).getX() + graphics.get(i).getWidth();
                }
                width = bufWidth - X;
                return width;
            }
        }
        return -1;
    }

    /**
     * Overrides the method of getHeight, to get height from element of ArrayList
     * @return height
     */
    @Override
    public double getHeight(){
        ArrayList<Graphics> graphics = getElements();
        if(graphics != null){
            if(!graphics.isEmpty()){
                double bufHeight = graphics.get(0).getHeight() + graphics.get(0).getY();
                for(int i = 0; i < graphics.size(); i++){
                    if(graphics.get(i).getHeight() + graphics.get(i).getX() > bufHeight)
                        bufHeight = graphics.get(i).getX() + graphics.get(i).getWidth();
                }
                height = bufHeight - Y;
                return height;
            }
        }
        return -1;
    }

    /**
     * Method of implementing a draw() in this class
     */
    @Override
    public void draw() {
        ArrayList<Graphics> graphics = getElements();
        for(int i = 0; i < graphics.size(); i++){
            graphics.get(i).draw();
        }
    }
}
