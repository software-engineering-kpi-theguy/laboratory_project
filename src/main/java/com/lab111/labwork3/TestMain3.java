package com.lab111.labwork3;

/**
 * Main class, where we testing a working of pattern
 * @author Yuriy
 * @version 1.2
 */
public class TestMain3 {
    public static void main(String[] args){
        Composite comp = new Composite();
        Dot dot1 = new Dot(0.145, 0.18);
        Line lne1 = new Line(0.76, 0.92, 0.2, 0.25);
        Ellipse eli1 = new Ellipse(0.5, 0.5, 0.2, 0.3);
        Line lne2 = new Line(0.74, 0.26, 0.85, 0.34);
        Rectangle rec1 = new Rectangle(0.4, 0.43, 0.53, -0.4);
        Dot dot2 = new Dot(0.36, 0.67);
        Ellipse eli2 = new Ellipse(-0.4, 0.23, 0.54, 0.18);
        Rectangle rec2 = new Rectangle(0.6, -0.5, 0.5, 0.7);
        comp.addGraphElements(dot1);
        comp.addGraphElements(lne1);
        comp.addGraphElements(eli1);
        comp.addGraphElements(lne2);
        comp.addGraphElements(rec1);
        comp.addGraphElements(dot2);
        comp.addGraphElements(eli2);
        comp.addGraphElements(rec2);
        comp.draw();
    }
}
