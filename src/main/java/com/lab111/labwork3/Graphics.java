package com.lab111.labwork3;

import java.util.ArrayList;

/**
 * The general abstract class of graphics, which describes all of characteristics of a vector field, and also a number of graphic objects
 * @author Yuriy
 * @version 1.2
 */
public abstract class Graphics {
    /**
     * @param X
     * Coordinate X, parameter of field
     * @param Y
     * Coordinate Y, parameter of field
     * @param width
     * Width from second coordinate X to first, or radius of X for ellipse, parameter of field
     * @param height
     * Height from second coordinate Y to first, or radius of Y for ellipse, parameter of field
     * @param elements
     * Number of graphic pictures, in this field
     */
    protected double X;
    protected double Y;
    protected double width;
    protected double height;
    protected ArrayList<Graphics> elements;

    /**
     * Getter of X
     * @return parameter X
     */
    public double getX() {
        return X;
    }

    /**
     * Setter of X
     * @param x
     * Sets the coordinate of X
     */
    public void setX(double x) {
        X = x;
    }

    /**
     * Getter of Y
     * @return parameter Y
     */
    public double getY() {
        return Y;
    }

    /**
     * Setter of Y
     * @param y
     * Sets the coordinate of Y
     */
    public void setY(double y) {
        Y = y;
    }

    /**
     * Getter of width
     * @return parameter width
     */
    public double getWidth() {
        return width;
    }

    /**
     * Setter of width
     * @param width
     * Sets the width of field
     */
    public void setWidth(double width) {
        this.width = width;
    }

    /**
     * Getter of height
     * @return parameter height
     */
    public double getHeight() {
        return height;
    }

    /**
     * Setter of height
     * @param height
     * Seyes the height of field
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * Getter of Elements ArrayList
     * @return elements ArrayList
     */
    public ArrayList<Graphics> getElements() {
        return elements;
    }

    /**
     * Method of initialization of ArrayList of Elements, before accessing its elements
     */
    public void initializeElements (){
        ArrayList<Graphics> elements = new ArrayList<>();
    }

    /**
     * Method of adding new element to ArrayList of Elements of graphics
     * @param a
     * Parameter of object of Graphics class, that adding to ArrayList
     */
    public void addGraphElements (Graphics a){
        if(elements != null){
            elements.add(a);
        }
    }

    /**
     * Method of removing specific element from ArrayList of Elements of graphics
     * @param a
     * Parameter of object of Graphics class, that removing from ArrayList
     */
    public void removeGraphElements (Graphics a){
        if(elements != null){
            elements.remove(a);
        }
    }

    /**
     * Abstract method draw() that will be inherited to other classes
     */
    public abstract void draw();
}
